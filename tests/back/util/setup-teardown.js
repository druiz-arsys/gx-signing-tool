import * as dotenv from 'dotenv'
import { beforeAll } from 'vitest'

function setupEnv() {
  const parsed = dotenv.config({ path: './tests/back/.env' }).parsed
  process.env = { ...process.env, ...parsed }
}

setupEnv()
beforeAll(setupEnv)
