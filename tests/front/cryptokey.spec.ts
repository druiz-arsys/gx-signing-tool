import { derivePublicKeyFromString, generatePrivateKey, privateKeyToPem, publicKeyToPem } from '../../web-crypto/cryptokey'
import { describe, it, expect } from 'vitest'

describe('key management', () => {
  it('should generate a private key', async () => {
    const privateKey = await generatePrivateKey()
    const privateKeyPem = await privateKeyToPem(privateKey)
    expect(privateKeyPem).toContain('-----BEGIN PRIVATE KEY-----')
  })

  it('should derive a valid public key from a private one', async () => {
    const { privateKey, publicKey } = await crypto.subtle.generateKey(
      {
        name: 'RSA-OAEP',
        modulusLength: 4096,
        publicExponent: new Uint8Array([1, 0, 1]),
        hash: 'SHA-256'
      },
      true,
      ['encrypt', 'decrypt']
    )
    const derivedPublicKey = await derivePublicKeyFromString((await privateKeyToPem(privateKey)) ?? '')
    const originPem = await publicKeyToPem(publicKey)
    expect(derivedPublicKey).toEqual(originPem)
  })
})

export {}
