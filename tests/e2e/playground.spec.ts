import { test, expect } from '@playwright/test'
import { getMonacoEditorValue, monacoEditorSelector, setMonacoEditorValue, verifyVCPresenceInHolder } from './util'

test.beforeEach(async ({ page }) => {
  await page.goto('/')
})

test.describe('Playground', () => {
  test('Disclaimer is visible and dismissible', async ({ page }) => {
    await expect(page.getByText('disclaimer')).toBeVisible()
    await page.getByRole('alert').getByLabel('Close').click()
    await expect(page.getByText('disclaimer')).not.toBeVisible()
  })
})

test.describe('Issue credentials', () => {
  test('Private key is set', async ({ page }) => {
    const editor = monacoEditorSelector(page, 'privateKey')
    await expect(editor).toContainText('-----BEGIN PRIVATE KEY-----')
  })

  test('Document name is generated', async ({ page }) => {
    await expect(page.getByPlaceholder('Document name')).toHaveValue(/[a-z]+/)
  })

  test('Can select VC example', async ({ page }) => {
    const issuer = page.locator('.issuer')
    const examples = issuer.getByTestId('examples')
    const options = examples.getByRole('option')
    const editor = monacoEditorSelector(page)
    await expect(editor).toBeVisible()
    await expect(options).toContainText(['Participant'])
    await expect(options).toContainText(['Service Offering'])
    await expect(options).toContainText(['Terms and Conditions'])
    await examples.selectOption('service')
    await expect(editor.getByRole('code')).toContainText('"type": "gx:ServiceOffering"')
  })

  test('Sign a Participant VC with custom DID', async ({ page }) => {
    const issuer = page.locator('.issuer')

    await expect(monacoEditorSelector(issuer)).toBeVisible()
    await issuer.getByTestId('examples').selectOption('participant')
    await issuer
      .locator('div')
      .filter({ hasText: /^Verification method$/ })
      .getByRole('textbox')
      .fill('did:web:example.com#verif1')

    const text = await getMonacoEditorValue(page, 0)
    const participant = JSON.parse(text ?? 'no json found')
    participant.id = 'https://example.com/credentials/3732'
    participant.credentialSubject.id = 'https://example.com/participants/3732#cs'
    participant.issuer = 'did:web:example.com'

    await setMonacoEditorValue(page, 0, JSON.stringify(participant, null, 2))
    const submit = issuer.getByRole('button', { name: 'Sign' })
    await submit.click()
    await expect(submit).toBeEnabled({ timeout: 20000 })
    await verifyVCPresenceInHolder(page, ['gx:LegalParticipant'])
  })

  test('Sign a Participant VC with Wizard issuer', async ({ page }) => {
    const issuer = page.locator('.issuer')
    await expect(monacoEditorSelector(issuer)).toBeVisible()
    await issuer.getByTestId('examples').selectOption('participant')
    await issuer.getByTestId('custom-issuer').click()
    await expect(issuer.getByTestId('custom-did-notice')).toBeVisible()
    const submit = issuer.getByRole('button', { name: 'Sign' })
    await submit.click()
    await expect(submit).toBeEnabled({ timeout: 20000 })
    await verifyVCPresenceInHolder(page, ['gx:LegalParticipant'])
  })
})
