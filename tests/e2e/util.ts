import { expect, Locator, type Page } from '@playwright/test'

export function monacoEditorSelector(locator: Locator | Page, testId = 'code-editor') {
  return locator.locator(`[data-testid="${testId}"][data-mounted="true"]`)
}

export async function getMonacoEditorValue(page: Page, index: number): Promise<string | undefined> {
  return await page.evaluate(async (i: number) => {
    while (!('monaco' in window && (window as any).monaco.editor.getEditors().length > 0)) {
      await new Promise(resolve => setTimeout(resolve, 100))
    }
    return (window as any).monaco.editor.getEditors()[i]?.getValue()
  }, index)
}

export async function setMonacoEditorValue(page: Page, index: number, value: string): Promise<void> {
  return await page.evaluate(
    async ({ i, v }) => {
      while (!('monaco' in window && (window as any).monaco.editor.getEditors().length > 0)) {
        await new Promise(resolve => setTimeout(resolve, 100))
      }
      ;(window as any).monaco.editor.getEditors()[i]?.setValue(v)
    },
    { i: index, v: value }
  )
}

export async function verifyVCPresenceInHolder(
  page: Page,
  expected = ['gx:LegalParticipant', 'gx:legalRegistrationNumber', 'gx:GaiaXTermsAndConditions']
) {
  const holder = page.locator('.holder')
  await Promise.all(expected.map(exp => expect(holder).toContainText(exp)))
}

export async function fillParticipantForm(
  cn: Locator | Page,
  values: {
    name: string
    registrationNumber: string
    headquarter: string
    legalAddress: string
    lrnType?: string
  }
) {
  await cn.getByLabel('name').fill(values.name)
  if (values.lrnType) {
    await cn.getByText(values.lrnType).click()
  }
  await cn.getByLabel('registration number').fill(values.registrationNumber)
  await cn.getByLabel('headquarter').fill(values.headquarter)
  await cn.getByLabel('legal address').fill(values.legalAddress)
}

export async function pickEnv(cn: Page | Locator, host = 'Gaia-x', version = 'development') {
  const clearingHouses = cn.getByLabel('clearing house')
  await expect(clearingHouses).toContainText(host)
  await clearingHouses.selectOption({ label: host })
  await cn.getByLabel('path').selectOption(version)
}

export async function getDocumentValue(page: Page, type: string, monacoEditorIndex: number) {
  const vc = page.getByTestId('documents').locator('div', { hasText: type })
  await vc.getByRole('button', { name: 'View' }).click()
  const modal = page.getByTestId('doc-view-modal')
  await expect(modal).toBeVisible()
  const editor = modal.locator(monacoEditorSelector(page))
  await expect(editor).toBeVisible()
  const rawValue = await getMonacoEditorValue(page, monacoEditorIndex)
  await modal.getByRole('button', { name: 'Close' }).first().click()
  return JSON.parse(rawValue ?? '{}')
}

export async function setEnv(page: Page, label = 'Gaia-x', version = 'development') {
  const clearingHouses = page.getByLabel('clearing house')
  await expect(clearingHouses).toContainText(label)
  await clearingHouses.selectOption({ label })
  await page.getByLabel(/(?:path|version)/i).selectOption(version)
}

export async function acceptTnc(cn: Page | Locator) {
  await expect(cn.getByText('The PARTICIPANT signing the Self-Description agrees as follows')).toBeVisible()
  await cn.getByTestId('agree-tnc').click()
}

export async function setPrivateKey(page: Page, editorIndex = 0, value = process.env.DID_X509_ROOT_PRIVATE_KEY?.replace(/\\+n/g, '\n') ?? '') {
  const editor = monacoEditorSelector(page, 'privateKey')
  await expect(editor).toBeVisible()
  await setMonacoEditorValue(page, editorIndex, value)
}
