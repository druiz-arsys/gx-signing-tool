import { VDocument } from '@/models/document'
import { signVerifiableCredential } from './signVerifiableCredential'
import { signVerifiablePresentation } from './signVerifiablePresentation'

export async function signDocumentWithKey(doc: any, privateKey: string, verificationMethodId: string): Promise<VDocument> {
  if (doc.type.includes('VerifiablePresentation')) {
    return await signVerifiablePresentation(privateKey, doc, verificationMethodId)
  }
  if (doc.type.includes('VerifiableCredential')) {
    return await signVerifiableCredential(privateKey, doc, verificationMethodId)
  }
  throw new Error('Unsupported document type')
}
