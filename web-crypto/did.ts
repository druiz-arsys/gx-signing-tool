import base58 from 'bs58'

export function publicKeyToDidId(publicKey: string) {
  return base58.encode(new TextEncoder().encode(publicKey))
}

export function getDidWeb(host: string, path: string) {
  return (
    'did:web:' +
    `${host}${path}`
      .replace(/http[s]?:\/\//, '')
      .replace(/\/+/g, '/')
      .replace(':', '%3A') // encode port ':' as '%3A' in did:web
      .replace(/\//g, ':')
  )
}

export function getDidWebUrl(did: string, protocol = 'https') {
  return protocol + '//' + did.substring(8).replaceAll(':', '/').replaceAll('%3A', ':') + '/did.json'
}
