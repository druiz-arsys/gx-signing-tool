import { importPKCS8 } from 'jose'

/**
 * Derive the public key from a private key
 * @param privateKey the private key to derive the public key from
 * @returns the public key as a string
 */
export async function derivePublicKeyFromString(privateKey: string): Promise<string> {
  const cryptoKey = await importPKCS8<CryptoKey>(privateKey, 'PS256', { extractable: true })
  const publicKey = await derivePublicKey(cryptoKey)
  return await publicKeyToPem(publicKey)
}

export async function generatePrivateKey() {
  const { privateKey } = await crypto.subtle.generateKey(
    {
      name: 'RSA-PSS',
      modulusLength: 2048,
      publicExponent: new Uint8Array([1, 0, 1]),
      hash: 'SHA-256'
    },
    true,
    ['sign', 'verify']
  )
  return privateKey
}

export async function derivePublicKey(privateKey: CryptoKey): Promise<CryptoKey> {
  const jwk = await crypto.subtle.exportKey('jwk', privateKey)

  // remove private data from JWK
  delete jwk.d
  delete jwk.dp
  delete jwk.dq
  delete jwk.q
  delete jwk.qi
  jwk.key_ops = ['verify']

  // import public key
  return await crypto.subtle.importKey(
    'jwk',
    jwk,
    {
      name: 'RSA-PSS',
      hash: { name: 'SHA-256' }
    },
    true,
    ['verify']
  )
}

export function arrayBufferToBase64(buffer: ArrayBuffer): string {
  let binary = ''
  const bytes = new Uint8Array(buffer)
  for (let i = 0; i < bytes.byteLength; i++) {
    binary += String.fromCharCode(bytes[i])
  }
  return (btoa as any)(binary)
}

export function stripPemHeaderAndFooter(pem: string) {
  let lines = pem.split('\n')
  lines = lines.slice(1, -1)

  return lines.join('\n')
}

export async function publicKeyToPem(publicKey: CryptoKey) {
  const spkiBuffer = await crypto.subtle.exportKey('spki', publicKey)
  let str = arrayBufferToBase64(spkiBuffer)
  let finalString = '-----BEGIN PUBLIC KEY-----\n'
  while (str.length > 0) {
    finalString += str.substring(0, 64) + '\n'
    str = str.substring(64)
  }
  finalString = finalString + '-----END PUBLIC KEY-----'
  return finalString
}

export async function privateKeyToPem(privateKey: CryptoKey) {
  const pkcs8Buffer = await crypto.subtle.exportKey('pkcs8', privateKey)
  const pemBody = arrayBufferToBase64(pkcs8Buffer).match(/.{1,64}/g)
  if (pemBody) return `-----BEGIN PRIVATE KEY-----\n${pemBody.join('\n')}\n-----END PRIVATE KEY-----`
}

export async function bufferToCryptoKey(buffer: BufferSource): Promise<CryptoKey> {
  return await crypto.subtle.importKey(
    'pkcs8',
    buffer,
    {
      name: 'RSA-PSS',
      hash: 'SHA-256'
    },
    true,
    ['sign']
  )
}
