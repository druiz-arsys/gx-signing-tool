import { ShapeName, VDocument } from './document'
import { GaiaXDeploymentPath } from '@/sharedTypes'

export interface StoredWalletEntry {
  key: string
  name: string
  type: ShapeName
  creation: string
  document: VDocument
  isVerified: GaiaXDeploymentPath | false
  privateKey?: string
  rawDocument?: string
}
