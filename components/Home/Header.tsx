import React, { useContext } from 'react'
import { Alert, Box, CloseButton, HStack, ListItem, Text, UnorderedList } from '@chakra-ui/react'
import { ConnectWallet } from '@/components/Home/ConnectWallet/ConnectWallet'
import { LocalWalletContext } from '@/contexts/LocalWalletContext'
import { useDisclaimer } from '@/hooks/useDisclaimer'
import { SelectClearingHouse } from '@/components/SelectClearingHouse'

export const Header: React.FC = () => {
  const { localWalletIsOpen } = useContext(LocalWalletContext)
  const [shouldDisplay, acceptDisclaimer] = useDisclaimer()

  return (
    <>
      <HStack justify={'flex-end'} alignItems={'flex-end'} flexWrap="wrap" py={5}>
        {!localWalletIsOpen && (
          <Box>
            <SelectClearingHouse />
          </Box>
        )}
        <Box>{!localWalletIsOpen && <ConnectWallet />}</Box>
      </HStack>
      {shouldDisplay && (
        <Alert position={'relative'} status="warning" borderRadius={4} mb={4} w={'35%'}>
          <Box>
            <Text fontWeight={'bold'}>Wizard disclaimer ! 🧙🏻‍</Text> This wizard has been developed for test purpose:
            <UnorderedList>
              <ListItem>It doesn't enforce key chain validation.</ListItem>
              <ListItem>It doesn't implement all classes validation.</ListItem>
              <ListItem>The provided keypair is for convenience. We advocate that you use your own.</ListItem>
            </UnorderedList>
          </Box>
          <CloseButton position="absolute" right="8px" top="8px" onClick={acceptDisclaimer} />
        </Alert>
      )}
    </>
  )
}
