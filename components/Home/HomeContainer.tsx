import React, { useContext } from 'react'
import { Header } from '@/components/Home/Header'
import { Box, Flex } from '@chakra-ui/react'
import { Issuer } from '@/components/Home/Issuer/Issuer'
import { Holder } from '@/components/Home/Holder/Holder'
import { LocalWallet } from '@/components/LocalWallet/LocalWallet'
import { LocalWalletContext } from '@/contexts/LocalWalletContext'

export const HomeContainer: React.FC = () => {
  const { localWalletIsOpen } = useContext(LocalWalletContext)

  return (
    <Box minH={'100vh'} px={[0, 8]} position={'relative'} bg={'transparent'}>
      <Header />
      <Flex direction={['column', 'column', 'column', 'row']} justifyContent={localWalletIsOpen ? 'flex-start' : 'space-around'}>
        {!localWalletIsOpen && <Issuer />}
        <Holder />
      </Flex>
      {localWalletIsOpen && <LocalWallet />}
    </Box>
  )
}
