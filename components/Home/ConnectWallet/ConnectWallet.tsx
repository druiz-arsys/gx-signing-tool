import {
  Box,
  Button,
  Grid,
  GridItem,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  useDisclosure
} from '@chakra-ui/react'
import React, { useContext } from 'react'
import { StaticImageData } from 'next/image'
import waltIdIcon from '../../../public/waltid.png'
import localWalletIcon from '../../../public/white-wallet.png'
import { WalletTile } from '@/components/Home/ConnectWallet/WalletTile'
import { LocalWalletContext } from '@/contexts/LocalWalletContext'
import { gradients } from '@/customTheme'

export type Wallet = {
  name: string
  icon: StaticImageData
  disabled: boolean
  openWallet: () => void
}

export const ConnectWallet: React.FC = () => {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const { isWalletConnected, setLocalWalletIsOpen, setIsWalletConnected } = useContext(LocalWalletContext)

  const handleOpenLocalWallet = () => {
    onClose()
    setLocalWalletIsOpen(true)
    setIsWalletConnected(true)
  }

  const walletsAvailable: Wallet[] = [
    {
      name: 'Local Wallet',
      icon: localWalletIcon,
      disabled: false,
      openWallet: handleOpenLocalWallet
    },
    {
      name: 'Walt.Id Wallet',
      icon: waltIdIcon,
      disabled: true,
      openWallet: () => null
    }
  ]

  return (
    <Box>
      {!isWalletConnected ? (
        <Button
          colorScheme={'green'}
          onClick={() => {
            onOpen()
          }}
        >
          Connect Wallet
        </Button>
      ) : (
        <Button colorScheme={'blue'} aria-label={'wallet'} onClick={() => setLocalWalletIsOpen(true)}>
          🟢<Text ml={2}>Local Wallet</Text>
        </Button>
      )}
      <Modal isCentered isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent bgGradient={gradients.primary} color={'white'}>
          <ModalHeader>Available Wallets</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Grid templateColumns="repeat(2, 1fr)" gap={4}>
              {walletsAvailable.map(wallet => (
                <GridItem key={wallet.name}>
                  <WalletTile wallet={wallet} />
                </GridItem>
              ))}
            </Grid>
          </ModalBody>
          <ModalFooter />
        </ModalContent>
      </Modal>
    </Box>
  )
}
