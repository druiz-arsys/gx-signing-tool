import { Box, Flex, Text, Tooltip } from '@chakra-ui/react'
import Image from 'next/image'
import React, { useMemo } from 'react'
import { Wallet } from '@/components/Home/ConnectWallet/ConnectWallet'

interface WalletTileProps {
  wallet: Wallet
}

const disabledStyleProps = {
  cursor: 'not-allowed',
  color: 'gray.500'
}

const enabledStyleProps = {
  color: 'gray.300',
  cursor: 'pointer',
  _hover: {
    color: 'white',
    backgroundColor: 'rgba(255, 255, 255, 0.2)'
  }
}

export const WalletTile: React.FC<WalletTileProps> = ({ wallet }) => {
  const getStyleProps = useMemo(() => {
    return wallet.disabled ? disabledStyleProps : enabledStyleProps
  }, [wallet.disabled])

  return (
    <Tooltip hasArrow label={wallet.disabled && 'Not available'}>
      <Flex
        h={'60px'}
        alignItems={'center'}
        justifyContent={'flex-start'}
        backgroundColor={'rgba(255, 255, 255, 0.1)'}
        py={3}
        px={1}
        borderRadius={'md'}
        onClick={wallet.openWallet}
        {...getStyleProps}
      >
        <Box mx={3}>
          <Image src={wallet.icon} width={'40'} alt={`${wallet.name} icon`} />
        </Box>
        <Text fontWeight={'bold'}>{wallet.name}</Text>
      </Flex>
    </Tooltip>
  )
}
