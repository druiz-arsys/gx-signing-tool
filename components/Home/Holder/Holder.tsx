import React, { useContext, useMemo } from 'react'
import { Flex, Heading, IconButton } from '@chakra-ui/react'
import { Container } from '../../ui/Container'
import { AppContext } from '@/contexts/AppContext'
import { ArrowBackIcon } from '@chakra-ui/icons'
import { LocalWalletContext } from '@/contexts/LocalWalletContext'
import { HolderColumn } from '@/components/Home/Holder/HolderColumn'

export const Holder: React.FC = () => {
  const { signedDocuments } = useContext(AppContext)
  const { localWalletIsOpen, setLocalWalletIsOpen } = useContext(LocalWalletContext)

  const documents = useMemo(() => {
    return Array.from(signedDocuments.values())
  }, [signedDocuments])

  return (
    <Container
      className={'holder'}
      minW={'40%'}
      maxW={localWalletIsOpen ? '100%' : ['100%', '100%', '1000px']}
      w={['auto', 'auto', 'auto', '60%']}
      minH={'70vh'}
    >
      <Flex fontSize={20} w="100%" alignItems={'center'} justifyContent={'flex-start'} p={3}>
        {localWalletIsOpen && (
          <IconButton
            variant={'outline'}
            aria-label={'Back'}
            icon={<ArrowBackIcon />}
            colorScheme={'white'}
            onClick={() => setLocalWalletIsOpen(false)}
          />
        )}
        <Heading ml={3} size={'md'} fontWeight={'bold'}>
          Holder
        </Heading>
      </Flex>
      <Flex w={'100%'} h={'100%'}>
        <HolderColumn documents={documents} title={''} />
      </Flex>
    </Container>
  )
}
