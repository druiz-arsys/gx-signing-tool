import React, { useEffect, useState } from 'react'
import {
  Badge,
  Box,
  Button,
  Checkbox,
  Divider,
  Flex,
  IconButton,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
  Text,
  Tooltip,
  useDisclosure
} from '@chakra-ui/react'
import { StoredWalletEntry } from '@/models/wallet'
import { CodeEditor } from '@/components/ui/CodeEditor'
import { CheckIcon, ChevronDownIcon, ChevronUpIcon, WarningIcon } from '@chakra-ui/icons'
import { CallCompliance } from './CallCompliance'
import { useVPValidation } from '@/hooks/useVPValidation'
import { gradients } from '@/customTheme'

interface PrepareComplianceCallProps {
  documents: StoredWalletEntry[]
}

export const PrepareComplianceCall: React.FC<PrepareComplianceCallProps> = ({ documents }) => {
  const { onOpen, onClose, isOpen } = useDisclosure()
  const callComplianceDisclosure = useDisclosure()
  const [isExpanded, setExpanded] = useState<string[]>([])
  const [selectedDocuments, setSelectedDocuments] = useState<StoredWalletEntry[]>([])
  const { isValid, validations } = useVPValidation(selectedDocuments)

  const selectableDocuments = documents.filter(d => !d.isVerified)

  useEffect(() => {
    const remains = selectedDocuments.filter(d => selectableDocuments.find(sd => sd.key === d.key))
    if (remains.length !== selectedDocuments.length) {
      setSelectedDocuments(remains)
    }
  }, [documents])

  const handleSubmitToCompliance = () => {
    onClose()
    callComplianceDisclosure.onOpen()
  }

  const editSelection = () => {
    callComplianceDisclosure.onClose()
    onOpen()
  }

  const toggleExpanded = (key: string) => {
    if (isExpanded.includes(key)) {
      setExpanded(isExpanded.filter(k => k !== key))
    } else {
      setExpanded([...isExpanded, key])
    }
  }

  const setDocumentSelected = (document: StoredWalletEntry, isSelected: boolean) => {
    if (isSelected) {
      setSelectedDocuments([...selectedDocuments, document])
    } else {
      setSelectedDocuments(selectedDocuments.filter(d => d.key !== document.key))
    }
  }

  return (
    <Box pb={1}>
      <Button colorScheme={'teal'} onClick={onOpen}>
        Call compliance
      </Button>
      <Modal isOpen={isOpen} onClose={onClose} size={'3xl'}>
        <ModalOverlay />
        <ModalContent color={'white'} bgGradient={gradients.primary}>
          <ModalHeader>Select credentials to present to compliance</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Flex gap={2} wrap={'wrap'} dir={'row'} mb={4}>
              {validations.map((v, i) => (
                <Tooltip
                  key={i}
                  label={
                    v.messages.length > 0 && (
                      <Stack>
                        {v.messages.map((m, ii) => (
                          <Text key={ii}>{m}</Text>
                        ))}
                      </Stack>
                    )
                  }
                >
                  <Flex
                    align={'center'}
                    flex={1}
                    justifyContent={'space-between'}
                    color={v.isValid ? 'green.100' : 'red'}
                    borderRadius={4}
                    fontWeight={v.isValid ? 'normal' : '500'}
                    bgColor={v.isValid ? '#642CF7' : 'red.100'}
                    fontSize={'sm'}
                    p={2}
                  >
                    <Flex align={'center'}>
                      <Badge>{v.min > 1 ? `${v.count}/${v.min}` : v.count}</Badge>
                      <Text ml={2}>{v.name}</Text>
                    </Flex>
                    <Text>{v.isValid ? <CheckIcon /> : <WarningIcon />}</Text>
                  </Flex>
                </Tooltip>
              ))}
            </Flex>
            {selectableDocuments.map((doc, i) => (
              <Box key={doc.key}>
                {i !== 0 && <Divider my={2} />}
                <Flex align={'center'} justifyContent={'space-between'} width={'100%'}>
                  <Checkbox onChange={e => setDocumentSelected(doc, e.target.checked)} isChecked={selectedDocuments.includes(doc)}>
                    <Box ml={2}>
                      <Text>{doc.name}</Text>
                      <Text>{doc.document?.credentialSubject?.type}</Text>
                    </Box>
                  </Checkbox>
                  <IconButton
                    onClick={() => toggleExpanded(doc.key)}
                    icon={isExpanded.includes(doc.key) ? <ChevronUpIcon boxSize={5} /> : <ChevronDownIcon boxSize={5} />}
                    aria-label="toggle document"
                    colorScheme={'blue'}
                    size="sm"
                  />
                </Flex>
                {isExpanded.includes(doc.key) && (
                  <Box mt={2}>
                    <CodeEditor value={JSON.stringify(doc.document, null, 2)} height={'200px'} readOnly />
                  </Box>
                )}
              </Box>
            ))}
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="teal" mr={3} onClick={handleSubmitToCompliance} isDisabled={!isValid}>
              Submit to compliance ({selectedDocuments.length})
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
      <Modal isOpen={callComplianceDisclosure.isOpen} onClose={callComplianceDisclosure.onClose} size={'3xl'}>
        <ModalOverlay />
        <ModalContent color={'white'} bgGradient={gradients.primary}>
          <ModalHeader>Submit a presentation to compliance</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <CallCompliance documents={selectedDocuments} onComplete={callComplianceDisclosure.onClose} onCancel={editSelection} />
          </ModalBody>
        </ModalContent>
      </Modal>
    </Box>
  )
}
