import React, { useContext } from 'react'
import { Button, Checkbox, Flex, Heading, HStack, Text, useDisclosure } from '@chakra-ui/react'
import { AppContext } from '@/contexts/AppContext'
import { StoredWalletEntry } from '@/models/wallet'
import { VerifiedByComplianceTag } from '@/components/VerifiedByComplianceTag'
import { LocalWalletContext } from '@/contexts/LocalWalletContext'
import { ViewDocumentModal } from '@/components/ViewDocumentModal'

interface SignedDocumentProps {
  id: string
  signedDocument: StoredWalletEntry
  onSelect?: () => void
  isSelected?: boolean
}

export const SignedDocument: React.FC<SignedDocumentProps> = ({ id, signedDocument, onSelect, isSelected }) => {
  const { isOpen, onOpen, onClose } = useDisclosure()

  const { handleImport, isWalletConnected } = useContext(LocalWalletContext)
  const { deleteDocument } = useContext(AppContext)

  const handleSave = () => {
    handleImport(signedDocument)
  }

  let type: string
  if (Array.isArray(signedDocument.document.credentialSubject)) {
    type = signedDocument.document.credentialSubject.map(s => s.type).join(', ')
  } else {
    type = signedDocument.document?.credentialSubject?.type || signedDocument.type
  }

  return (
    <Flex backgroundColor={`rgba(255, 255, 255, ${isSelected ? 0.5 : 0.2})`} p={3} borderRadius={'md'} w={'100%'} h={'100%'}>
      <Flex flexDirection={'column'} justifyContent={'space-between'} w={'100%'}>
        <Flex w={'100%'} justifyContent={'space-between'} alignItems={'center'}>
          <Flex>
            {onSelect && <Checkbox mr={2} isChecked={isSelected} onChange={onSelect} />}
            <Heading size={'sm'}>{signedDocument.name}</Heading>
          </Flex>
          {!!signedDocument.isVerified && <VerifiedByComplianceTag version={signedDocument.isVerified} />}
        </Flex>
        <Text mt={3}>{type}</Text>
        <HStack mt={3}>
          <Button colorScheme={'blue'} onClick={onOpen}>
            View
          </Button>
          <Button colorScheme={'red'} onClick={() => deleteDocument(id)}>
            Delete
          </Button>
          <Button isDisabled={!isWalletConnected} colorScheme={'green'} onClick={handleSave}>
            Save
          </Button>
        </HStack>
      </Flex>
      <ViewDocumentModal
        docName={signedDocument.name}
        document={signedDocument.document}
        rawDocument={signedDocument.rawDocument}
        isOpen={isOpen}
        onClose={onClose}
      />
    </Flex>
  )
}
