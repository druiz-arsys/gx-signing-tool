import { Flex } from '@chakra-ui/react'
import React, { useContext } from 'react'
import { LocalWalletHeader } from '@/components/LocalWallet/LocalWallerHeader'
import { LocalWalletContent } from '@/components/LocalWallet/LocalWalletContent'
import { LocalWalletContext } from '@/contexts/LocalWalletContext'
import { LocalWalletToolbar } from '@/components/LocalWallet/LocalWalletToolbar'
import JSZip from 'jszip'
import { saveAs } from 'file-saver'

export const LocalWallet: React.FC = () => {
  const { setLocalWalletIsOpen } = useContext(LocalWalletContext)
  const { selectedDocuments } = useContext(LocalWalletContext)

  const createZip = () => {
    const zip = new JSZip()
    const jsonFiles = selectedDocuments.map((entry, i) => ({
      filename: entry.name === 'Document' ? `Document-${i + 1}.json` : `${entry.name}.json`,
      content: JSON.stringify(entry.document, null, 2)
    }))

    jsonFiles.forEach(jsonFile => {
      zip.file(jsonFile.filename, jsonFile.content)
    })

    zip.generateAsync({ type: 'blob' }).then(content => {
      saveAs(content, 'credentials.zip')
    })
  }

  return (
    <Flex
      direction={'column'}
      alignItems={'center'}
      position={'absolute'}
      zIndex={2}
      top={['80px', '80px', '80px', '10px']}
      right={'10px'}
      minW={['auto', 'auto', 'auto', 'auto', 'auto', '500px']}
      minH={'800px'}
      maxH={'100vh'}
      p={3}
      color={'white'}
      bgGradient={'linear(to-br, rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.3))'}
      borderRadius={'md'}
      boxShadow={'lg'}
    >
      <LocalWalletHeader onClose={() => setLocalWalletIsOpen(false)} />
      <LocalWalletToolbar onDownload={createZip} />
      <LocalWalletContent />
    </Flex>
  )
}
