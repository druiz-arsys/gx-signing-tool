import React, { useContext } from 'react'
import { Button, Checkbox, Flex, Text } from '@chakra-ui/react'
import { DownloadIcon } from '@chakra-ui/icons'
import { LocalWalletContext } from '@/contexts/LocalWalletContext'
import { useLocalWallet } from '@/hooks/useLocalWallet'
import { ImportDocument } from '@/components/Home/Holder/ImportDocument'

interface LocalWalletToolbarProps {
  onDownload: () => void
}

export const LocalWalletToolbar: React.FC<LocalWalletToolbarProps> = ({ onDownload }) => {
  const { selectedDocuments, setSelectedDocuments } = useContext(LocalWalletContext)
  const { walletEntries } = useLocalWallet()

  const handleSelectAll = () => {
    const isAllSelected = selectedDocuments.length === walletEntries.length
    if (isAllSelected) {
      setSelectedDocuments([])
    } else {
      setSelectedDocuments(walletEntries)
    }
  }

  return (
    <Flex flexDirection={'column'} w={'100%'} alignItems={'flex-end'} px={5}>
      <Flex justifyContent={'space-between'} w={'100%'}>
        <ImportDocument saveInWallet />
        <Button
          isDisabled={!selectedDocuments.length}
          aria-label={'Download as json'}
          colorScheme={'green'}
          leftIcon={<DownloadIcon />}
          onClick={onDownload}
        >
          Download JSON
        </Button>
      </Flex>
      <Flex w={'100%'} mt={4} justifyContent={'space-between'}>
        <Flex>
          <Checkbox onChange={handleSelectAll} mr={2} />
          <Text>
            {selectedDocuments.length} document{selectedDocuments.length > 1 ? 's' : ''} selected
          </Text>
        </Flex>
        <Text>{walletEntries.length} documents</Text>
      </Flex>
    </Flex>
  )
}
