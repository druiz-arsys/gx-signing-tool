import React from 'react'
import { CloseButton, Divider, Flex, Heading } from '@chakra-ui/react'

interface LocalWalletHeaderProps {
  onClose: () => void
}
export const LocalWalletHeader: React.FC<LocalWalletHeaderProps> = ({ onClose }) => {
  return (
    <Flex direction={'column'} w={'100%'} alignItems={'center'}>
      <Flex justifyContent={'space-between'} w={'100%'}>
        <Heading size={'lg'} ml={3}>
          Local Wallet
        </Heading>
        <CloseButton color={'white'} onClick={onClose} />
      </Flex>
      <Divider my={3} w={'100%'} />
    </Flex>
  )
}
