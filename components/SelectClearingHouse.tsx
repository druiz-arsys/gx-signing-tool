import React, { useContext } from 'react'
import { FormControl, FormLabel, HStack, Select } from '@chakra-ui/react'
import { capitalize } from '@/utils/util'
import { GaiaXDeploymentPath } from '@/sharedTypes'
import { ClearingHousesContext } from '@/contexts/ClearingHousesContext'

export const SelectClearingHouse: React.FC = () => {
  const {
    providers,
    gxDeploymentPaths,
    setSelectedClearingHouse,
    getClearingHouseByName,
    setSelectedGxDeploymentPath,
    selectedGxDeploymentPath,
    selectedClearingHouse
  } = useContext(ClearingHousesContext)

  const handleSelectClearingHouse = (provider: string) => {
    const clearingHouse = getClearingHouseByName(provider)
    if (clearingHouse) setSelectedClearingHouse(clearingHouse)
  }

  const handleSelectGxDeploymentPath = (version: GaiaXDeploymentPath) => setSelectedGxDeploymentPath(version)

  return (
    <HStack alignItems={'flex-end'} justifyContent={'flex-end'}>
      <FormControl color={'white'} w={'auto'}>
        {gxDeploymentPaths ? <FormLabel>Environment</FormLabel> : <FormLabel>Clearing House</FormLabel>}
        <Select w={'160px'} value={selectedClearingHouse.name} onChange={e => handleSelectClearingHouse(e.target.value)}>
          {providers.map(provider => (
            <option key={provider} value={provider}>
              {capitalize(provider)}
            </option>
          ))}
        </Select>
      </FormControl>
      <FormControl color={'white'} w={'auto'}>
        {gxDeploymentPaths ? <FormLabel>Path</FormLabel> : <FormLabel>Version</FormLabel>}
        <Select
          w={'160px'}
          value={gxDeploymentPaths ? selectedGxDeploymentPath : 'v1'}
          onChange={e => handleSelectGxDeploymentPath(e.target.value as GaiaXDeploymentPath)}
        >
          {gxDeploymentPaths ? (
            gxDeploymentPaths.map(version => (
              <option key={version} value={version}>
                {version}
              </option>
            ))
          ) : (
            <option key={'v1'} value={'v1'}>
              {'v1'}
            </option>
          )}
        </Select>
      </FormControl>
    </HStack>
  )
}
