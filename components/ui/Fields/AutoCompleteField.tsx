import React from 'react'
import { FormField } from '@/components/ui/Fields/FormField'
import { DataListEntry } from '@/models/dataListEntry'

export interface AutoCompleteFieldProps {
  label: string
  name: string
  datalistEntries: DataListEntry[]
  placeholder?: string
  required?: boolean
}

export const AutoCompleteField: React.FC<AutoCompleteFieldProps> = ({ label, placeholder, required = false, name, datalistEntries }) => {
  return (
    <fieldset>
      <FormField name={name} label={label} list={name + '-list'} placeholder={placeholder} autocomplete={'off'} required={required} />
      <datalist id={name + '-list'}>
        {datalistEntries.map(entry => {
          return (
            <option value={entry.value} key={entry.key}>
              {entry.key}
            </option>
          )
        })}
      </datalist>
    </fieldset>
  )
}
