import { FormControl, FormLabel, Input, InputGroup, InputRightElement } from '@chakra-ui/react'
import React from 'react'
import { RepeatIcon } from '@chakra-ui/icons'
import { FormFieldProps } from '@/components/ui/Fields/FormField'
import { Field } from 'formik'
import { generateName } from '@/utils/util'

export const DocumentNameField: React.FC<FormFieldProps> = ({ required = false, name }) => {
  const handleGenerateName = (setFieldValue: (name: string, value: string) => void) => {
    setFieldValue(name, generateName())
  }

  return (
    <Field name={name}>
      {({ field, form }: any) => (
        <FormControl isRequired={required}>
          <FormLabel>Document name</FormLabel>
          <InputGroup>
            <Input {...field} />
            <InputRightElement width="4.5rem">
              <RepeatIcon cursor={'pointer'} aria-label={'Generate a name'} onClick={() => handleGenerateName(form.setFieldValue)}></RepeatIcon>
            </InputRightElement>
          </InputGroup>
        </FormControl>
      )}
    </Field>
  )
}
