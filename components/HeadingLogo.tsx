import NextLink from 'next/link'
import { Box, BoxProps, Flex, Heading } from '@chakra-ui/react'
import Image from 'next/image'
import gxLogo from '@/public/gx-logo.png'
import React from 'react'

interface HeadingLogoProps extends BoxProps {
  size: string
}

export const HeadingLogo: React.FC<HeadingLogoProps> = props => {
  return (
    <Flex alignItems={'center'} as={NextLink} href={'/'} {...props}>
      <Box mx={4}>
        <Image src={gxLogo} alt="logo" width={50} height={50} />
      </Box>
      <Heading color={'white'} size={props.size}>
        Gaia-X Wizard
      </Heading>
    </Flex>
  )
}
