import React, { ReactNode } from 'react'
import {
  Box,
  BoxProps,
  Drawer,
  DrawerContent,
  Flex,
  FlexProps,
  Icon,
  IconButton,
  Link,
  useColorModeValue,
  useDisclosure,
  VStack
} from '@chakra-ui/react'
import { FiCompass, FiHome, FiMenu, FiTrendingUp, FiZap } from 'react-icons/fi'
import { IconType } from 'react-icons'
import { HeadingLogo } from '@/components/HeadingLogo'
import { FaGitlab } from 'react-icons/fa'
import NextLink from 'next/link'

interface LinkItemProps {
  name: string
  href: string
  icon: IconType
  isExternal?: boolean
}

const LinkItems: Array<LinkItemProps> = [
  { name: 'Playground', icon: FiHome, href: '/' },
  { name: 'Onboarding', icon: FiZap, href: '/onboarding' },
  { name: 'Stepper', icon: FiTrendingUp, href: '/stepper' },
  { name: 'Get Legal Registration Number', icon: FiCompass, href: '/legalRegistrationNumber' },
  { name: 'Contribute', icon: FaGitlab, href: 'https://gitlab.com/projects/44670736', isExternal: true }
]

export const Sidebar: React.FC<{ children: ReactNode }> = ({ children }) => {
  const { isOpen, onOpen, onClose } = useDisclosure()
  return (
    <Box minH="100vh">
      <SidebarContent onClose={() => onClose} display={{ base: 'none', md: 'block' }} bg={'transparent'} color={'white'} />
      <Drawer autoFocus={false} isOpen={isOpen} placement="left" onClose={onClose} returnFocusOnClose={false} onOverlayClick={onClose} size="full">
        <DrawerContent>
          <SidebarContent onClose={onClose} />
        </DrawerContent>
      </Drawer>
      {/* mobilenav */}
      <MobileNav display={{ base: 'flex', md: 'none' }} onOpen={onOpen} />
      <Box ml={{ base: 0, md: 60 }}>{children}</Box>
    </Box>
  )
}

interface SidebarProps extends BoxProps {
  onClose: () => void
}

const SidebarContent = ({ ...rest }: SidebarProps) => {
  return (
    <Box
      bg={useColorModeValue('white', 'gray.900')}
      borderRight="1px"
      borderRightColor={'rgba(255, 255, 255, 0.3)'}
      w={{ base: 'full', md: 60 }}
      pos="fixed"
      h="full"
      pt={5}
      {...rest}
    >
      <HeadingLogo color={'black'} size={'md'} />
      <VStack align={'flex-start'} mt={5}>
        {LinkItems.map(link => (
          <NavItem key={link.name} href={link.href} icon={link.icon} isExternal={link.isExternal}>
            {link.name}
          </NavItem>
        ))}
      </VStack>
    </Box>
  )
}

interface NavItemProps extends FlexProps {
  icon: IconType
  href: string
  children: ReactNode
  isExternal?: boolean
}

const NavItem = ({ icon, href, children, isExternal, ...rest }: NavItemProps) => {
  return (
    <Link as={NextLink} isExternal={isExternal} href={href} style={{ textDecoration: 'none' }} _focus={{ boxShadow: 'none' }} width={'100%'}>
      <Flex
        align="center"
        p="4"
        role="group"
        cursor="pointer"
        _hover={{
          bg: 'rgba(255, 255, 255, 0.1)',
          color: 'white'
        }}
        fontWeight={'bold'}
        {...rest}
      >
        {icon && (
          <Icon
            mr="4"
            fontSize="16"
            _groupHover={{
              color: 'white'
            }}
            as={icon}
          />
        )}
        {children}
      </Flex>
    </Link>
  )
}

interface MobileProps extends FlexProps {
  onOpen: () => void
}

const MobileNav = ({ onOpen, ...rest }: MobileProps) => {
  return (
    <Flex
      ml={{ base: 0, md: 60 }}
      px={{ base: 4, md: 24 }}
      height="20"
      alignItems="center"
      bg={'transparent'}
      borderBottomWidth="1px"
      borderBottomColor={useColorModeValue('gray.200', 'gray.700')}
      justifyContent="flex-start"
      color={'white'}
      {...rest}
    >
      <IconButton variant="outline" onClick={onOpen} aria-label="open menu" icon={<FiMenu />} color={'white'} />

      <HeadingLogo size={'xl'} />
    </Flex>
  )
}
