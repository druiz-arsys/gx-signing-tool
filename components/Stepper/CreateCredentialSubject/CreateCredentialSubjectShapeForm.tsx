import React from 'react'
import { ParticipantForm } from '@/components/Stepper/CreateCredentialSubject/ParticipantForm/ParticipantForm'
import { ServiceOfferingForm } from '@/components/Stepper/CreateCredentialSubject/ServiceOfferingForm/ServiceOfferingForm'
import { ExampleTuple } from '@/sharedTypes'
import { TermsAndConditionsForm } from './TermsAndConditionsForm/TermsAndConditionsForm'

interface CreateCredentialSubjectPropsShapeFormProps {
  onComplete: (newFormValues: any) => void
  onPrevious: () => void
  selectedExample: ExampleTuple
}

export const CreateCredentialSubjectShapeForm: React.FC<CreateCredentialSubjectPropsShapeFormProps> = ({
  onComplete,
  onPrevious,
  selectedExample
}) => {
  const type = selectedExample.shape
  switch (type) {
    case 'participant':
      return <ParticipantForm onComplete={onComplete} onPrevious={onPrevious} />
    case 'service':
      return <ServiceOfferingForm onComplete={onComplete} onPrevious={onPrevious} />
    case 'termsAndConditions':
      return <TermsAndConditionsForm onComplete={onComplete} onPrevious={onPrevious} />
  }
  return null
}
