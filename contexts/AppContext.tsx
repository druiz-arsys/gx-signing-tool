import React, { createContext, useState } from 'react'
import { v4 as uuidv4 } from 'uuid'
import type { StoredWalletEntry } from '@/models/wallet'
import type { StorableVDocument } from '@/models/document'
import { GaiaXDeploymentPath } from '@/sharedTypes'

export interface AppContextProps {
  signedDocuments: Map<string, StoredWalletEntry>
  addDocument: (doc: StorableVDocument, isVerified?: false | GaiaXDeploymentPath) => void
  deleteDocument: (key: string) => void
}

export const AppContext = createContext<AppContextProps>({
  signedDocuments: new Map(),
  addDocument: () => null,
  deleteDocument: () => null
})

export const AppProvider = ({ children }: { children: React.ReactNode }) => {
  const [signedDocuments, setSignedDocuments] = useState<Map<string, StoredWalletEntry>>(new Map())

  const addDocument = (doc: StorableVDocument, isVerified: false | GaiaXDeploymentPath = false) => {
    const documentUuid = doc.key || uuidv4()
    setSignedDocuments(
      new Map(
        signedDocuments.set(documentUuid, {
          key: documentUuid,
          name: doc.docName || 'Document',
          creation: new Date().toISOString(),
          type: doc.document.type,
          privateKey: doc.privateKey,
          document: doc.document,
          rawDocument: doc.rawDocument,
          isVerified
        })
      )
    )
  }

  const deleteDocument = (key: string) => {
    const newMap = new Map(signedDocuments)
    newMap.delete(key)
    setSignedDocuments(newMap)
  }

  return (
    <AppContext.Provider
      value={{
        signedDocuments,
        addDocument,
        deleteDocument
      }}
    >
      {children}
    </AppContext.Provider>
  )
}
