import React, { createContext, Dispatch, SetStateAction, useContext, useState } from 'react'
import { AppContext } from '@/contexts/AppContext'
import { useToast } from '@chakra-ui/react'
import { useLocalWallet } from '@/hooks/useLocalWallet'
import { StoredWalletEntry } from '@/models/wallet'

interface LocalWalletContextProps {
  handleExport: (document: StoredWalletEntry) => void
  handleImport: (document: StoredWalletEntry, confirmationAlert?: boolean) => void
  handleDelete: (documentId: string) => void
  localWalletIsOpen: boolean
  setLocalWalletIsOpen: (isOpen: boolean) => void
  savedDocuments: StoredWalletEntry[]
  selectedDocuments: StoredWalletEntry[]
  setSelectedDocuments: Dispatch<SetStateAction<StoredWalletEntry[]>>
  isWalletConnected: boolean
  setIsWalletConnected: Dispatch<SetStateAction<boolean>>
}

export const LocalWalletContext = createContext<LocalWalletContextProps>({
  handleExport: () => null,
  handleImport: () => null,
  localWalletIsOpen: false,
  setLocalWalletIsOpen: () => null,
  savedDocuments: [],
  isWalletConnected: false,
  handleDelete: () => null,
  setIsWalletConnected: () => null,
  selectedDocuments: [],
  setSelectedDocuments: () => null
})

interface LocalWalletProviderProps {
  children: React.ReactNode
}

export const LocalWalletProvider: React.FC<LocalWalletProviderProps> = ({ children }) => {
  const toast = useToast()
  const { addDocument } = useContext(AppContext)
  const [localWalletIsOpen, setLocalWalletIsOpen] = useState<boolean>(false)
  const { walletEntries, persistWalletEntry, deleteWalletEntry, isWalletConnected, setIsWalletConnected } = useLocalWallet()

  const [savedDocuments, setSavedDocuments] = useState<StoredWalletEntry[]>(walletEntries)
  const [selectedDocuments, setSelectedDocuments] = useState<StoredWalletEntry[]>([])

  const handleExport = (document: StoredWalletEntry) => {
    addDocument({
      docName: document.name,
      document: document.document,
      privateKey: document.privateKey || '',
      key: document.key,
      rawDocument: document.rawDocument
    })
  }

  const handleImport = async (document: StoredWalletEntry, confirmationAlert = true) => {
    await persistWalletEntry(document)
    const index = savedDocuments.findIndex(d => d.key === document.key)
    if (index === -1) {
      setSavedDocuments([...savedDocuments, document])
    } else {
      const newDocs = [...savedDocuments]
      newDocs[index] = document
      setSavedDocuments(newDocs)
    }
    if (confirmationAlert) {
      toast({
        title: 'Document saved',
        description: 'Document saved in wallet',
        status: 'success',
        duration: 3000,
        isClosable: true
      })
    }
  }

  const handleDelete = async (documentId: string) => {
    await deleteWalletEntry(documentId)
    const newSavedDocuments = savedDocuments.filter(document => document.key !== documentId)
    setSavedDocuments(newSavedDocuments)

    const newSelectedDocuments = selectedDocuments.filter(document => document.key !== documentId)
    setSelectedDocuments(newSelectedDocuments)
  }

  return (
    <LocalWalletContext.Provider
      value={{
        selectedDocuments,
        setSelectedDocuments,
        handleExport,
        localWalletIsOpen,
        setLocalWalletIsOpen,
        handleImport,
        savedDocuments,
        isWalletConnected,
        setIsWalletConnected,
        handleDelete
      }}
    >
      {children}
    </LocalWalletContext.Provider>
  )
}
