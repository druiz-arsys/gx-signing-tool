import type { AppProps } from 'next/app'
import { AppProvider } from '@/contexts/AppContext'
import { Box, ChakraProvider } from '@chakra-ui/react'
import { LocalWalletProvider } from '@/contexts/LocalWalletContext'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { Sidebar } from '@/components/Sidebar'
import { customTheme, gradients } from '@/customTheme'
import { ClearingHousesProvider } from '@/contexts/ClearingHousesContext'

const queryClient = new QueryClient()

export default function App({ Component, pageProps }: AppProps) {
  return (
    <ChakraProvider theme={customTheme}>
      <QueryClientProvider client={queryClient}>
        <AppProvider>
          <LocalWalletProvider>
            <ClearingHousesProvider>
              <Box bgGradient={gradients.primary}>
                <Sidebar>
                  <Component {...pageProps} />
                </Sidebar>
              </Box>
            </ClearingHousesProvider>
          </LocalWalletProvider>
        </AppProvider>
      </QueryClientProvider>
    </ChakraProvider>
  )
}
