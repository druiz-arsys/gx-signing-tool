import { HomeContainer } from '@/components/Home/HomeContainer'
import { SignatureProvider } from '@/contexts/SignatureContext'

export default function Home() {
  return (
    <SignatureProvider defaultIsCustomIssuer={true}>
      <HomeContainer />
    </SignatureProvider>
  )
}
