import type { NextApiRequest, NextApiResponse } from 'next'
import { credentialsService } from '../service/providers.service'

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  try {
    if (req.method === 'GET') {
      const did = (req.url?.split('/').pop() ?? '').split('?')[0]
      const vcid = (req.url ?? '').split(/vcid(?:=|%3D)/).pop() ?? ''
      const relativeVcId = did + vcid
      res.status(200).send(JSON.stringify([await credentialsService.getVC(relativeVcId)], null, 2))
    } else {
      res.status(405).send(req.method + ' not allowed')
    }
  } catch (error: any) {
    console.log(error)
    res.status(error['statusCode'] ?? 400).send(error)
  }
}
