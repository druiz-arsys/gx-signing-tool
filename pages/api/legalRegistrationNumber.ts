import type { NextApiRequest, NextApiResponse } from 'next'
import { credentialsService, didService, notaryService } from '@/pages/api/service/providers.service'
import Joi from 'joi'
import { JsonLdDocument } from 'jsonld'
import { getString, joinPathParts } from '@/utils/util'

const VC_PARAM = 'vcid'
const ID_PARAM = 'id'

export default async function handler(req: NextApiRequest, res: NextApiResponse<JsonLdDocument | string>) {
  if (req.method === 'POST') {
    const { error } = validateBody(req.body)
    if (error) return res.status(400).send('Bad request')

    if (!req.query[VC_PARAM]) return res.status(400).send('Bad request')

    const decodedVcId = Buffer.from(req.query.vcid as string, 'base64').toString('utf-8')

    try {
      const clearingHouse = req.body.clearingHouse
      const notaryVC = await notaryService.postLegalRegistrationNumberVC(decodedVcId, req.body.values, clearingHouse)

      if (req.body.stored && ID_PARAM in req.query) {
        await handleSave(req, JSON.stringify(notaryVC))
      }

      res.send(notaryVC)
    } catch (e: any) {
      return res.status(400).send('Invalid registration number')
    }
  } else {
    return res.status(405).send(req.method + ' not allowed')
  }
}

async function handleSave(req: NextApiRequest, vc: string) {
  const didId = didService.pathToDid(req.query[ID_PARAM])
  const vcId = getString(req.query[VC_PARAM])
  if (VC_PARAM in req.query && vcId) {
    if (!('no-did-update' in req.query)) {
      await credentialsService.addVCToDidDoc(didId, vcId, joinPathParts(req.query.id))
    }
    return credentialsService.storeVC(didId + vcId, JSON.parse(vc))
  }
  return await credentialsService.storeDidDoc(
    didId,
    {
      publicKeyPem: vc,
      certchainUri: getString(req.query.certchainUri)
    },
    !('storeless' in req.query)
  )
}

function validateBody(input: any) {
  const schema = Joi.object({
    stored: Joi.boolean().required(),
    values: Joi.object({
      '@context': Joi.array().items(Joi.string()).required(),
      id: Joi.string().required(),
      type: Joi.string().required()
    })
      .pattern(Joi.string().valid('gx:taxID', 'gx:EUID', 'gx:EORI', 'gx:vatID', 'gx:leiCode'), Joi.string().required())
      .required(),
    clearingHouse: Joi.string()
  })

  return schema.validate(input)
}
