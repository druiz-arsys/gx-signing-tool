import crypto from 'crypto'
import { NextApiRequest, NextApiResponse } from 'next'

export default function handler(req: NextApiRequest, res: NextApiResponse<number[] | { message: string }>) {
  try {
    if (req.method !== 'GET') {
      return res.status(405).json({ message: 'Method Not Allowed' })
    }

    const randomBytes = crypto.randomBytes(32)
    const randomNumbers = Array.from(randomBytes)

    res.status(200).json(randomNumbers)
  } catch (error: any) {
    console.log(error)
    res.status(error['statusCode'] ?? 500).json({ message: 'An error occurred on the server' })
  }
}
