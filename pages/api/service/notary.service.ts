import axios from 'axios'
import { JsonLdDocument } from 'jsonld'

export class NotaryService {
  private readonly notaryLRNEndpoint: (vcId: string, clearingHouse?: string) => string
  private readonly notaryUrl: string

  constructor() {
    if (!process.env.REGISTRATION_NUMBER_NOTARY_BASE_URL) throw new Error('Missing REGISTRATION_NUMBER_NOTARY_BASE_URL env variable')
    this.notaryUrl = process.env.REGISTRATION_NUMBER_NOTARY_BASE_URL

    this.notaryLRNEndpoint = (vcId, clearingHouse) => {
      if (!!clearingHouse) {
        return `https://${clearingHouse}/registrationNumberVC?vcid=${encodeURIComponent(vcId)}`
      } else return `${this.notaryUrl}/registrationNumberVC?vcid=${encodeURIComponent(vcId)}`
    }
  }

  async postLegalRegistrationNumberVC(vcId: string, verifiableCredential: any, clearingHouse?: string): Promise<JsonLdDocument> {
    const { data } = await axios.post(this.notaryLRNEndpoint(vcId, clearingHouse), verifiableCredential, {
      headers: {
        'Content-Type': 'application/json'
      }
    })
    return data
  }
}
