import base58 from 'bs58'
import forge from 'node-forge'
import { StorageService } from './storage.service'
import { joinPath } from '@/utils/util'

function parseProcessPem(pem: string): string {
  return pem?.replaceAll(/\\?\\n/g, '\n')
}

export class KeychainService {
  private readonly host: string
  private readonly x509Root: string
  private readonly privateKey: forge.pki.rsa.PrivateKey
  private readonly intermediateCert: forge.pki.Certificate

  constructor(
    private storageService: StorageService,
    private keychainPath = process.env.KEYCHAIN_ENDPOINT_PATH ?? 'api/x509'
  ) {
    if (!process.env.BASE_URL) throw new Error('Missing BASE_URL env variable')
    this.host = process.env.BASE_URL

    if (!process.env.DID_X509_ROOT_CERTIFICATE) throw new Error('Missing DID_X509_ROOT_CERTIFICATE env variable')
    this.x509Root = parseProcessPem(process.env.DID_X509_ROOT_CERTIFICATE)
    if (!this.x509Root) throw new Error('DID_X509_ROOT_CERTIFICATE env variable empty')

    if (!process.env.DID_X509_ROOT_PRIVATE_KEY) throw new Error('Missing DID_X509_ROOT_PRIVATE_KEY env variable')
    const rootPrivateKey = parseProcessPem(process.env.DID_X509_ROOT_PRIVATE_KEY)
    if (!rootPrivateKey) throw new Error('DID_X509_ROOT_PRIVATE_KEY env variable empty')

    this.intermediateCert = forge.pki.certificateFromPem(this.x509Root)
    this.privateKey = forge.pki.privateKeyFromPem(rootPrivateKey)
  }

  async createX509(
    id: string,
    publicKeyPem: string,
    opts?: {
      from?: Date
      to?: Date
      serialNumber?: string
    }
  ): Promise<{ url: string; x509: string }> {
    const publicKey = forge.pki.publicKeyFromPem(publicKeyPem)

    // Build CSR
    const csr = forge.pki.createCertificationRequest()
    csr.publicKey = publicKey
    csr.setSubject([
      { name: 'commonName', value: id },
      { name: 'countryName', value: 'BE' },
      { name: 'organizationName', value: 'Gaia-X AISBL' }
    ])
    csr.sign(this.privateKey)

    // Generate certificate from CSR
    const cert = forge.pki.createCertificate()
    cert.setSubject(this.intermediateCert.subject.attributes)
    cert.setIssuer(this.intermediateCert.subject.attributes)
    cert.validity.notBefore = opts?.from ?? new Date()
    cert.validity.notAfter = opts?.to ?? futureDate(cert.validity.notBefore)
    cert.publicKey = csr.publicKey
    cert.serialNumber = opts?.serialNumber ?? forge.util.bytesToHex(forge.random.getBytesSync(16))
    cert.setExtensions([
      {
        name: 'basicConstraints',
        cA: false
      }
    ])
    cert.sign(this.privateKey)

    const intermediateCertPem = forge.pki.certificateToPem(this.intermediateCert)
    const pemCert = forge.pki.certificateToPem(cert)

    const x509Chain = `${pemCert}${intermediateCertPem}${this.x509Root}`

    const x509Name = this.getX509Name(id)

    await this.storageService.upload(x509Chain, x509Name, 'x509')

    return {
      url: joinPath(this.host, this.keychainPath, x509Name + '.pem'),
      x509: x509Chain
    }
  }

  getX509Name(id: string) {
    return base58.encode(new TextEncoder().encode(id))
  }

  getX509ById(id: string): Promise<string | undefined> {
    return this.getX509ByName(this.getX509Name(id))
  }

  getX509ByName(x509Name: string): Promise<string | undefined> {
    return this.storageService.download(x509Name, 'x509')
  }
}

function futureDate(src: Date, addYears = 1) {
  const target = new Date(src)
  target.setFullYear(src.getFullYear() + addYears)
  return target
}
