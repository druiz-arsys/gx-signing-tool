import { KeychainService } from './keychain.service'
import * as jose from 'jose'
import { getDidWeb } from '@/web-crypto/did'
import { joinPath, joinPathParts } from '@/utils/util'
import { JsonLdDocument } from 'jsonld'

export interface DidDocRequest {
  publicKeyPem: string
  certchainUri?: string
}

export class DidService {
  private readonly registryUrl: string
  private readonly didUrl: string
  private readonly host: string

  constructor(
    private keychainService: KeychainService,
    private didPath = process.env.DID_ENDPOINT_PATH ?? '/api/credentials',
    private registryPath = process.env.REGISTRY_ENDPOINT_PATH ?? '/api/registry'
  ) {
    if (!process.env.BASE_URL) throw new Error('Missing BASE_URL env variable')
    this.host = process.env.BASE_URL

    this.registryUrl = joinPath(this.host, this.registryPath)
    this.didUrl = joinPath(this.host, this.didPath)
  }

  async createDidDocument(id: string, req: DidDocRequest): Promise<JsonLdDocument> {
    const spki = await jose.importSPKI(req.publicKeyPem, 'PS256', {
      extractable: true
    })
    const verificationMethodIdentifier = `${id}#JWK2020`
    return {
      '@context': ['https://www.w3.org/ns/did/v1'],
      id,
      verificationMethod: [
        {
          '@context': 'https://w3id.org/security/suites/jws-2020/v1',
          id: verificationMethodIdentifier,
          type: 'JsonWebKey2020',
          publicKeyJwk: {
            alg: 'PS256',
            ...(await jose.exportJWK(spki)),
            x5u: req.certchainUri || (await this.keychainService.createX509(id, req.publicKeyPem)).url
          }
        }
      ],
      assertionMethod: [verificationMethodIdentifier],
      service: [
        // https://www.w3.org/TR/did-core/#dfn-service
        {
          id: `${id}#vcregistry`,
          type: 'CredentialRegistry', // https://www.w3.org/TR/did-spec-registries/#credentialregistry
          serviceEndpoint: `${this.registryUrl}/{credentialSubject.id}`
        }
      ]
    }
  }

  pathToDid(path: string | string[] | undefined): string {
    if (path == null) {
      throw new Error('Missing path')
    }
    let joinedPath = joinPathParts(path)
    if (joinedPath.endsWith('/did.json')) {
      joinedPath = joinedPath.substring(0, joinedPath.length - 9)
    }
    return getDidWeb(this.didUrl, '/' + joinedPath)
  }
}
