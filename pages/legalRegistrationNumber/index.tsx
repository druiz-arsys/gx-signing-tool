import { LegalRegistrationNumberContainer } from '@/components/LegalRegistrationNumber/LegalRegistrationNumberContainer'
import { SignatureProvider } from '@/contexts/SignatureContext'

export default function LegalRegistrationNumber() {
  return (
    <SignatureProvider defaultIsCustomIssuer={false}>
      <LegalRegistrationNumberContainer />
    </SignatureProvider>
  )
}
