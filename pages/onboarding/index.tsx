import { OnboardingStepperContainer } from '@/components/Stepper/OnboardingStepperContainer'
import { SignatureProvider } from '@/contexts/SignatureContext'
import { StepperProvider } from '@/contexts/StepperContext'

export default function Onboarding() {
  return (
    <SignatureProvider defaultIsCustomIssuer={false} isSignMultiVC={true}>
      <StepperProvider allRequiredVCs={true}>
        <OnboardingStepperContainer />
      </StepperProvider>
    </SignatureProvider>
  )
}
