import { generateSlug, RandomWordOptions } from 'random-word-slugs'
import { GaiaXDeploymentPath, VerifiableCredential } from '@/sharedTypes'
import * as jose from 'jose'

export function joinPathParts(path: string | string[] | undefined): string {
  return Array.isArray(path) ? path.join('/') : path || ''
}

export function joinPath(host: string, ...path: string[]): string {
  if (!path.length) {
    return host
  }
  const p = path[0]
  let resolved: string
  if (host.endsWith('/')) {
    resolved = p.startsWith('/') ? host + p.substring(1) : host + p
  } else {
    resolved = p.startsWith('/') ? host + p : host + '/' + p
  }
  if (path.length === 1) {
    return resolved
  }
  return joinPath(resolved, ...path.slice(1))
}

export const generateName = () => {
  const options: RandomWordOptions<2> = {
    format: 'kebab',
    partsOfSpeech: ['adjective', 'noun'],
    categories: {
      adjective: ['color', 'appearance'],
      noun: ['animals', 'food']
    }
  }

  return generateSlug(2, options)
}

export const toObjectPath = (path: string, object: any) => {
  const parts = path.split('.')
  let current = object

  for (const part of parts) {
    current = current[part]

    // Exit the loop if the path isn't found in the object
    if (current === undefined) {
      break
    }
  }

  return current
}

export function getString(param: string | string[] | undefined): string | undefined {
  return Array.isArray(param) ? param?.[0] : param
}

export const isDocumentVerified = (document: VerifiableCredential) => {
  if (document.issuer.includes('did:web:compliance.lab.gaia-x.eu')) {
    return document.issuer.split(':').pop() as GaiaXDeploymentPath
  } else return false
}

export function capitalize(inputString: string): string {
  if (inputString.length === 0) {
    return inputString
  }

  const firstLetter = inputString.charAt(0).toUpperCase()
  const restOfString = inputString.slice(1)

  return firstLetter + restOfString
}

export const parseJWT = (jwt: string) => {
  return jose.decodeJwt(jwt)
}
