import { StoredWalletEntry } from '@/models/wallet'
import { useState, useEffect } from 'react'

export interface VPValidation {
  name: string
  isValid: boolean
  count: number
  min: number
  messages: string[]
}

export interface UseVPValidation {
  validations: VPValidation[]
  isValid: boolean
}

function limitString(str: string, limit = 10): string {
  if (str.length > limit) {
    return `${str.substring(0, limit / 2)}...${str.substring(str.length - limit / 2)}`
  }
  return str
}

export const useVPValidation = (documents: StoredWalletEntry[]): UseVPValidation => {
  const [validations, setValidations] = useState<VPValidation[]>([])
  const [isValid, setIsValid] = useState<boolean>(false)

  useEffect(() => {
    const byType = documents.reduce(
      (acc, d) => {
        const type = d.document.credentialSubject.type
        if (!acc[type]) {
          acc[type] = []
        }
        acc[type].push(d)
        return acc
      },
      {} as { [key: string]: StoredWalletEntry[] }
    )
    const count = (type: string) => byType[type]?.length ?? 0

    const issuers = Array.from(
      new Set(
        documents
          // No need for LRN since they are signed by Gaia-X
          .filter(d => d.document.credentialSubject.type !== 'gx:legalRegistrationNumber')
          .map(d => d.document.issuer)
      )
    )
    const signedTnCIssuers = (byType['gx:GaiaXTermsAndConditions'] ?? []).map(tnc => tnc.document.issuer)
    const missingTnCs = issuers.filter(issuer => !signedTnCIssuers.includes(issuer))

    const participantCount = count('gx:LegalParticipant')
    const lRNCount = count('gx:legalRegistrationNumber')
    const tncCount = count('gx:GaiaXTermsAndConditions')

    setValidations([
      {
        name: 'Participant',
        isValid: participantCount >= 1,
        count: participantCount,
        min: 1,
        messages: []
      },
      {
        name: 'Terms and conditions',
        isValid: tncCount > 0 && missingTnCs.length === 0,
        count: tncCount,
        min: Math.max(1, issuers.length),
        messages: missingTnCs.map(issuer => `You must sign terms and conditions for issuer ${limitString(issuer, 30)}`)
      },
      {
        name: 'Legal registration number',
        isValid: lRNCount > 0 && lRNCount === participantCount,
        count: lRNCount,
        min: Math.max(1, participantCount),
        messages: []
      }
    ])
  }, [documents])

  useEffect(() => {
    setIsValid(!validations.find(v => !v.isValid))
  }, [validations])

  return {
    validations,
    isValid
  }
}
