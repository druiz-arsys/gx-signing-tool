import { ClaimNotaryVC } from '@/components/LegalRegistrationNumber/LegalRegistrationNumberContainer'
import { environment, getRootUrl } from '@/env'
import { joinPath } from '@/utils/util'
import { derivePublicKeyFromString } from '@/web-crypto/cryptokey'
import { publicKeyToDidId } from '@/web-crypto/did'
import { createCredentialSubjectId } from '../useGXSignature'
import axios from 'axios'
import { v4 as uuidv4 } from 'uuid'
import { useContext } from 'react'
import { ClearingHousesContext } from '@/contexts/ClearingHousesContext'

export interface UseLRN {
  createLRN: (type: string, value: string, vcId?: string, csId?: string) => Promise<any>
}

export const useLRN = (getPrivateKey: () => Promise<string>, isCustomIssuer: boolean): UseLRN => {
  const { selectedClearingHouse } = useContext(ClearingHousesContext)
  const buildLRNRequest = async (
    type: string,
    value: string,
    id?: string
  ): Promise<{ credentialSubject: ClaimNotaryVC; vcId: string | undefined }> => {
    const credentialSubject: any = {
      '@context': ['https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/participant'],
      type: 'gx:legalRegistrationNumber',
      [`gx:${type}`]: value,
      id
    }

    let vcId: string | undefined
    if (!isCustomIssuer) {
      const didId = publicKeyToDidId(await derivePublicKeyFromString(await getPrivateKey()))
      const URIPath = joinPath(getRootUrl(location.pathname), 'api', 'legalRegistrationNumber', didId)
      delete credentialSubject.id
      credentialSubject.id = await createCredentialSubjectId(
        joinPath(location.origin, URIPath),
        credentialSubject,
        encodeURIComponent(`${type}=${value}`)
      )
      vcId = await createCredentialSubjectId(joinPath(location.origin, URIPath), credentialSubject, uuidv4())
    }

    return { credentialSubject, vcId }
  }

  const createLRN = async (type: string, value: string, vcId?: string, csId?: string) => {
    const { credentialSubject, vcId: providedVCId } = await buildLRNRequest(type, value, csId)
    const encodedVcId = Buffer.from((providedVCId || vcId) ?? '').toString('base64')
    const { data } = await axios.post(environment(window.location.href).requestNotaryRegistrationNumberEndpoint(encodedVcId, csId), {
      stored: !isCustomIssuer,
      clearingHouse: selectedClearingHouse.registrationNotaryEndpoint,
      values: credentialSubject
    })
    return data
  }

  return {
    createLRN
  }
}
