import { useEffect, useState } from 'react'

type UseDisclaimerReturnsValues = [boolean, () => void]

export const useDisclaimer = (): UseDisclaimerReturnsValues => {
  const [shouldDisplayDisclaimer, setShouldDisplayDisclaimer] = useState(false)

  useEffect(() => {
    const hasAcceptedDisclaimer = sessionStorage.getItem(`disclaimer`)
    if (!!hasAcceptedDisclaimer) {
      setShouldDisplayDisclaimer(false)
    } else {
      setShouldDisplayDisclaimer(true)
    }
  }, [])

  const acceptDisclaimer = () => {
    sessionStorage.setItem('disclaimer', 'true')
    setShouldDisplayDisclaimer(false)
  }

  return [shouldDisplayDisclaimer, acceptDisclaimer]
}
