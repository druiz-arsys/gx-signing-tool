import { useState } from 'react'

export interface UseIssuer {
  issuer: string
  isValid: boolean
  verificationMethod: string
  setVerificationMethod: (verificationMethod: string) => void
  isCustomIssuer: boolean
  setIssuer: (issuer: string) => void
  setIsCustomIssuer: (isCustomIssuer: boolean) => void
}

export const useIssuer = (defaultIsCustomIssuer = false): UseIssuer => {
  const [issuer, setIssuer] = useState('')
  const [verificationMethod, setVerificationMethod] = useState('')
  const [isCustomIssuer, setIsCustomIssuer] = useState(defaultIsCustomIssuer)

  return {
    issuer,
    isCustomIssuer,
    verificationMethod,
    setVerificationMethod,
    setIssuer,
    setIsCustomIssuer,
    isValid: !isCustomIssuer || !!verificationMethod
  }
}
