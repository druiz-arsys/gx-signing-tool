import { derivePublicKeyFromString } from '@/web-crypto/cryptokey'
import { getDidWeb, publicKeyToDidId } from '@/web-crypto/did'
import { useCredentials } from './useCredentials'
import { joinPath } from '@/utils/util'
import { getRootUrl } from '@/env'
import { signDocumentWithKey } from '@/web-crypto/signDocument'

interface SignAndUploadDocumentOptions {
  privateKey: string
  verificationMethod: string
  docToSign: any
  docName: string
}

interface UseGXSignatureReturnValues {
  signAndUploadDocument: (opts: SignAndUploadDocumentOptions) => Promise<any>
}

export const useGXSignature = (): UseGXSignatureReturnValues => {
  const { createDid, uploadVC } = useCredentials()

  const signAndUploadDocument = async (opts: SignAndUploadDocumentOptions) => {
    const publicKey = await derivePublicKeyFromString(opts.privateKey)
    const didURIPath = joinPath(getRootUrl(location.pathname), 'api', 'credentials', publicKeyToDidId(publicKey))
    const didUrl = joinPath(location.origin, didURIPath)
    const didDoc = await createDid(publicKey)

    if (!didDoc) throw new Error('DID document creation failed')
    if (!(didDoc as any).verificationMethod?.length) throw new Error('No verification method found')

    const doc = {
      ...opts.docToSign,
      id: didUrl + '?vcid=' + encodeURIComponent(opts.docName),
      issuer: getDidWeb(location.host, didURIPath),
      credentialSubject: { ...(opts.docToSign.credentialSubject ?? {}) }
    }
    doc.credentialSubject.id = await createCredentialSubjectId(didUrl, doc.credentialSubject, doc.id)

    const signedDocument = await signDocumentWithKey(doc, opts.privateKey, (didDoc as any).verificationMethod[0].id)
    await uploadVC(signedDocument as any)
    return signedDocument
  }

  return {
    signAndUploadDocument
  }
}

export async function createCredentialSubjectId(didWeb: string, credentialSubject: any, docId: string) {
  const encoder = new TextEncoder()
  const data = encoder.encode(JSON.stringify(credentialSubject))
  const key = await crypto.subtle.importKey('raw', encoder.encode(docId), { name: 'HMAC', hash: 'SHA-256' }, false, ['sign'])
  const hmac = Array.from(new Uint8Array(await crypto.subtle.sign('HMAC', key, data)))
    .map(b => b.toString(16).padStart(2, '0'))
    .join('')
  return didWeb + '#' + hmac
}

export async function createVcId(did: string, docName: string) {
  return did + '?vcid=' + encodeURIComponent(docName)
}
