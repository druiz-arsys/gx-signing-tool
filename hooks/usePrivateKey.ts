import { getRootUrl } from '@/env'
import { joinPath } from '@/utils/util'
import { derivePublicKeyFromString, generatePrivateKey, privateKeyToPem } from '@/web-crypto/cryptokey'
import { getDidWeb, publicKeyToDidId } from '@/web-crypto/did'
import { useEffect, useState } from 'react'
import { useWebAuthn } from './useWebAuthn'

export interface PrivateKeyUse {
  privateKey: string
  setPrivateKey: (privateKey: string, store?: boolean) => void
  derivePublicKey: () => Promise<string>
  buildWizardDidWeb: () => Promise<string>
  getPrivateKey: () => Promise<string>
}

export const usePrivateKey = (): PrivateKeyUse => {
  const [privateKey, setPrivateKey] = useState('')
  const { decryptKey, isWebauthnEnabled } = useWebAuthn()

  const updateKey = (newKey: string, store = !!newKey) => {
    if (store) {
      localStorage.setItem('privateKey', newKey)
    }
    setPrivateKey(newKey)
  }

  const getPemKeyAndUpdate = async () => {
    const key = await generatePrivateKey()
    const pemKey = await privateKeyToPem(key)
    if (pemKey) updateKey(pemKey)
  }

  useEffect(() => {
    const key = localStorage.getItem('privateKey')
    if (key) {
      setPrivateKey(key)
    } else {
      getPemKeyAndUpdate()
    }
  }, [])

  const derivePublicKey = async (): Promise<string> => {
    const privateKey = await getPrivateKey()
    return await derivePublicKeyFromString(privateKey)
  }

  const buildWizardDidWeb = async () =>
    getDidWeb(location.host, joinPath(getRootUrl(location.pathname), 'api', 'credentials', publicKeyToDidId(await derivePublicKey())))

  const getPrivateKey = async () => {
    if (isWebauthnEnabled()) {
      const key = await decryptKey(privateKey)
      if (!key) throw new Error('Could not decrypt key')
      return key
    }
    return privateKey
  }

  return {
    privateKey,
    setPrivateKey: updateKey,
    derivePublicKey,
    buildWizardDidWeb,
    getPrivateKey
  }
}
