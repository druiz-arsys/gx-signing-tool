import { useToast } from '@chakra-ui/react'

export const useErrorHandler = () => {
  const toast = useToast()

  const errorHandler = (error: Error, message?: string) => {
    toast({
      title: 'Error',
      description: message || error.message,
      status: 'error',
      duration: 5000,
      isClosable: true
    })

    console.log(error)
  }

  return { errorHandler }
}
