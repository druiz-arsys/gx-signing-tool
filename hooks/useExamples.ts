import { useQuery } from '@tanstack/react-query'
import axios from 'axios'
import { ExampleTuple } from '@/sharedTypes'
import { useState } from 'react'
import { useErrorHandler } from '@/hooks/useError'
import VPTemplate from '@/data/doc/VPTemplate.json'
import { environment } from '@/env'

const fetchExamples = async () => {
  const { data: fetchedExamples } = await axios.get<ExampleTuple[]>(environment(window.location.href).requestExamplesEndpoint)
  return fetchedExamples
}

export const useExamples = () => {
  const [selectedExample, setSelectedExample] = useState<ExampleTuple | undefined>()
  const { errorHandler } = useErrorHandler()

  const query = useQuery({
    queryKey: ['examples'],
    queryFn: fetchExamples,
    onError: (error: Error) => errorHandler(error, 'Could not fetch examples'),
    initialData: [],
    refetchOnWindowFocus: false
  })

  const getExampleByKey = (key: string): ExampleTuple | undefined => {
    return query.data.find(example => example.shape === key)
  }

  return {
    ...query,
    selectedExample,
    setSelectedExample,
    getExampleByKey,
    VPTemplate
  }
}
