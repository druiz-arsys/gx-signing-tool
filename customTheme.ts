import { extendTheme } from '@chakra-ui/react'

const customColors = {
  gxBlue: '#000094',
  darkBlue: '#000071',
  mediumBlue: '#465AFF',
  purple: '#B900FF',
  turquoise: '#46DAFF'
}

export const gradients = {
  primary: `linear(to-br, ${customColors.purple} -5%, ${customColors.gxBlue} 85%, ${customColors.turquoise} 110%)`
}

export const customTheme = extendTheme({
  colors: { ...customColors }
})
