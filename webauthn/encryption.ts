export async function registerWebAuthn(challenge: Uint8Array): Promise<PublicKeyCredential | undefined> {
  const supported = await checkForBrowserSupport()
  if (!supported) {
    throw new Error('Not supported')
  }

  const salt = getSaltFromLocalstorage()
  const credential = (await createCredential(salt, challenge)) as PublicKeyCredential
  if (credential) {
    setCredentialIdInLocalstorage(credential.id)

    return credential
  }
}

export async function encrypt(challenge: Uint8Array, plainText: BufferSource): Promise<ArrayBuffer | undefined> {
  const encryptionKey = await loginWebAuthn(challenge)
  const nonce = getNonceFromLocalstorage()

  if (encryptionKey && nonce) {
    return await crypto.subtle.encrypt({ name: 'AES-GCM', iv: nonce }, encryptionKey, plainText)
  }
}

export async function decrypt(challenge: Uint8Array, cipherText: BufferSource): Promise<ArrayBuffer | undefined> {
  const encryptionKey = await loginWebAuthn(challenge)
  const nonce = getNonceFromLocalstorage()

  if (encryptionKey && nonce) {
    return await crypto.subtle.decrypt({ name: 'AES-GCM', iv: nonce }, encryptionKey, cipherText)
  }
}

export function unregisterWebAuthn(): void {
  localStorage.removeItem('credentialId')
  localStorage.removeItem('salt')
  localStorage.removeItem('userId')
}

export function isCredentialIdSet(): boolean {
  return !!localStorage.getItem('credentialId')
}

export async function checkForBrowserSupport(): Promise<boolean> {
  if (navigator.credentials && window.PublicKeyCredential) {
    const available = await PublicKeyCredential.isUserVerifyingPlatformAuthenticatorAvailable()
    if (available) {
      console.log('Supported.')
    } else {
      throw new Error('WebAuthn supported, Platform Authenticator *not* supported.')
    }
    return true
  } else {
    throw new Error('Not supported.')
  }
}

async function loginWebAuthn(challenge: Uint8Array): Promise<CryptoKey | undefined> {
  const salt = getSaltFromLocalstorage()
  const credential = (await getCredential(salt, challenge)) as PublicKeyCredential
  if (credential) {
    const extensionResults = credential.getClientExtensionResults()
    const cryptoKey = await getEncryptionKey(extensionResults)
    return cryptoKey
  }
}

async function createCredential(salt: Uint8Array, challenge: Uint8Array): Promise<Credential | null> {
  const userId = getUserIdFromLocalstorage()

  const credentialId = getCredentialIdFromLocalstorage()

  // typecasting needed because the typings are not up to date on this experimental prf extension
  const extensionClientInputs = {
    prf: {
      eval: {
        first: salt.buffer
      }
    }
  } as AuthenticationExtensionsClientInputs

  return await navigator.credentials.create({
    publicKey: {
      challenge: challenge,
      rp: {
        name: 'Gaia-x'
      },
      user: {
        id: userId,
        name: 'Gaia-x Signing Tool',
        displayName: 'Gaia-x Signing Tool'
      },
      pubKeyCredParams: [
        { alg: -8, type: 'public-key' }, // Ed25519
        { alg: -7, type: 'public-key' }, // ES256
        { alg: -257, type: 'public-key' } // RS256
      ],
      excludeCredentials: [
        {
          id: credentialId?.buffer ?? new Uint8Array(0).buffer,
          type: 'public-key'
        }
      ],
      authenticatorSelection: {
        userVerification: 'preferred'
      },
      extensions: extensionClientInputs
    }
  })
}

async function getCredential(salt: Uint8Array, challenge: Uint8Array): Promise<Credential | null> {
  const credentialId = getCredentialIdFromLocalstorage()
  if (!credentialId) {
    throw new Error('No credential id found')
  }

  // typecasting needed because the typings are not up to date on this experimental prf extension
  const extensionClientInputs = {
    prf: {
      eval: {
        first: salt.buffer
      }
    }
  } as AuthenticationExtensionsClientInputs

  return await navigator.credentials.get({
    publicKey: {
      challenge: challenge,
      userVerification: 'preferred',
      allowCredentials: [
        {
          id: credentialId.buffer,
          type: 'public-key'
        }
      ],
      extensions: extensionClientInputs
    }
  })
}

async function getEncryptionKey(extensionResults: AuthenticationExtensionsClientOutputs): Promise<CryptoKey> {
  // typecasting needed because the typings are not up to date on this experimental prf extension
  const inputKeyMaterial = new Uint8Array((extensionResults as any).prf.results.first)
  const inputKey = await crypto.subtle.importKey('raw', inputKeyMaterial, 'HKDF', false, ['deriveKey'])
  const salt = getSaltFromLocalstorage()
  const label = 'encryption key'
  const info = new TextEncoder().encode(label)

  return await crypto.subtle.deriveKey({ name: 'HKDF', info, salt, hash: 'SHA-256' }, inputKey, { name: 'AES-GCM', length: 256 }, false, [
    'encrypt',
    'decrypt'
  ])
}

function getSaltFromLocalstorage(): Uint8Array {
  const salt = localStorage.getItem('salt')
  if (salt) {
    return new Uint8Array(JSON.parse(salt))
  } else {
    const newSalt = new Uint8Array(32)
    crypto.getRandomValues(newSalt)
    localStorage.setItem('salt', JSON.stringify(Array.from(newSalt)))
    return newSalt
  }
}

function getNonceFromLocalstorage(): Uint8Array {
  const nonce = localStorage.getItem('nonce')
  if (nonce) {
    return new Uint8Array(JSON.parse(nonce))
  } else {
    const newNonce = new Uint8Array(12)
    crypto.getRandomValues(newNonce)
    localStorage.setItem('nonce', JSON.stringify(Array.from(newNonce)))
    return newNonce
  }
}

function getUserIdFromLocalstorage(): Uint8Array {
  const userId = localStorage.getItem('userId')
  if (userId) {
    return new Uint8Array(JSON.parse(userId))
  } else {
    const newUserId = new Uint8Array(16)
    crypto.getRandomValues(newUserId)
    localStorage.setItem('userId', JSON.stringify(Array.from(newUserId)))
    return newUserId
  }
}

function getCredentialIdFromLocalstorage(): Uint8Array | null {
  const credentialId = localStorage.getItem('credentialId')
  if (credentialId) {
    return new Uint8Array(Buffer.from(JSON.parse(credentialId), 'base64'))
  } else {
    return null
  }
}

function setCredentialIdInLocalstorage(credentialId: string): void {
  localStorage.setItem('credentialId', JSON.stringify(credentialId))
}
