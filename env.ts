import { joinPath } from './utils/util'

const envs = ['development']

export const environment = (locationHref: string) => {
  locationHref = getRootUrl(locationHref)
  return {
    requestComplianceEndpoint: (vcId?: string) =>
      joinPath(locationHref, 'api', 'credential-offers') + (vcId ? '?vcid=' + encodeURIComponent(vcId) : ''),

    requestExamplesEndpoint: joinPath(locationHref, 'api', 'getExamples'),
    requestDidEndpoint: (didId: string) => joinPath(locationHref, 'api', 'credentials', didId),
    requestVCEndpoint: (vcId: string) => joinPath(locationHref, 'api', 'credentials', vcId),
    requestWebauthnChallengeEndpoint: () => joinPath(locationHref, 'api', 'webauthn', 'challenge'),
    appPath: getRootUrl(locationHref).replace(/^https?:\/\/((?!\/).)+\/?/, ''),
    requestNotaryRegistrationNumberEndpoint: (vcId: string, didId?: string) =>
      joinPath(locationHref, 'api', 'legalRegistrationNumber') + '?vcid=' + vcId + (!!didId ? '&?id=' + didId : '')
  }
}

export function getRootUrl(url: string): string {
  const regex = new RegExp(`^(https?:\/\/[^/]+)`)
  const matches = url.match(regex)
  const rootUrl = matches ? matches[1] : ''

  for (const env of envs) {
    if (url.startsWith(`${rootUrl}/${env}`)) {
      return `${rootUrl}/${env}`
    }
  }

  if (url.startsWith(rootUrl)) {
    return ''
  }

  return rootUrl
}
